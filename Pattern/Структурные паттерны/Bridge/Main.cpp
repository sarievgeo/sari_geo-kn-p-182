#include<iostream>
#include<string>

using namespace std;

class WorkProcess
{
public:
    virtual void execute() = 0;
};

class Calculate : public WorkProcess
{
public:
    void execute()
    {
        cout << "Calculate data.\n";
    }
};

class Store : public WorkProcess
{
public:
    void execute()
    {
        cout << "Store data.\n";
    }
};

class Component
{
public:
    virtual void WhatToDo() = 0;
};

class Videocard : public Component
{
private:
    WorkProcess* workprocess;
public:
    Videocard(WorkProcess* obj) : workprocess(obj){}
    void WhatToDo()
    {
        workprocess->execute;
    }
};

class CPU : public Component
{
private:
    WorkProcess* workprocess;
public:
    CPU(WorkProcess* obj) : workprocess(obj) {}
    void WhatToDo()
    {
        workprocess->execute;
    }

};

class RAM : public Component
{
private:
    WorkProcess* workprocess;
public:
    RAM(WorkProcess* obj) : workprocess(obj) {}
    void WhatToDo()
    {
        workprocess->execute;
    }
};

void main()
{
    WorkProcess* calculate = new Calculate();
    WorkProcess* store = new Store();

    Component* first = new CPU(calculate);
    Component* second = new Videocard(calculate);
    Component* third = new RAM(store);
}