#include<iostream>
using namespace std;

class PowerSupply
{
private:

public:
	void toFeed()
	{
		cout << "Power Supply: to feed with power supply" << endl;
	}
	void toFeedOnVideocard()
	{
		cout << "Power Supply: to feed on videocard" << endl;
	}
	void toFeedRAM()
	{
		cout << "Power Supply: to feed RAM" << endl;
	}
	void toFeedOpticalDiscReader()
	{
		cout << "Power Supply: to feed optical disc reader" << endl;
	}
	void toFeedHDD()
	{
		cout << "Power Supply: to feed HDD" << endl;
	}
	void stoptToFeedOnVideocard()
	{
		cout << "Power Supply: stop to feed on videocard" << endl;
	}
	void stopToFeedRAM()
	{
		cout << "Power Supply: stop to feed RAM" << endl;
	}
	void stopToFeedOpticalDiscReader()
	{
		cout << "Power Supply: stop to feed optical disc reader" << endl;
	}
	void stopToFeedHDD()
	{
		cout << "Power Supply: stop to feed HDD" << endl;
	}
	void PowerSupplyOff()
	{
		cout << "Power Supply: off" << endl;
	}
};

class Sensors
{
private:

public:
	void checkVoltage()
	{
		cout << "Sensors: check voltage" << endl;
	}
	void checkTemperaturePowerSupply()
	{
		cout << "Sensors: check temperature power supply" << endl;
	}
	void checkTemperatureVideocard()
	{
		cout << "Sensors: check temperature videocard" << endl;
	}
	void checkTemperatureRAM()
	{
		cout << "Sensors: check temperature RAM" << endl;
	}
	void checkTemperatureAllSysteams()
	{
		cout << "Sensors: check temperature all systeams" << endl;
	}

};

class Videocard
{
private:

public:
	void launch() 
	{
		 cout << "videocard: launch videocard" <<  endl;
	}
	void checkCommunicationMonitor() 
	{
		 cout << "videocard: check communication monitor" <<  endl;
	}
	void outputDataRAM() 
	{
		 cout << "videocard: output aata RAM" <<  endl;
	}
	void displayDriveInformation() 
	{
		 cout << "videocard: display drive information" <<  endl;
	}
	void outputLaunchHDD() 
	{
		 cout << "videocard: output launch HDD" <<  endl;
	}
	void oupputFarewellMessage() 
	{
		 cout << "videocard: good buy" <<  endl;
	}
};

class RAM
{
private:

public:
	void launchDevices() 
	{
		 cout << "RAM: launch devices" <<  endl;
	}
	void memoryAnalysis() 
	{
		 cout << "RAM: memory analysis" <<  endl;
	}
	void memoryClearing() 
	{
		 cout << "RAM: memory clearing" <<  endl;
	}
};

class HDD
{
private:

public:
	void launch() 
	{
		 cout << "HDD: launch HDD" <<  endl;
	}
	void checkBootSector() 
	{
		 cout << "HDD: check boot sector" <<  endl;
	}
	void deviceStop() 
	{
		 cout << "HDD: device stop" <<  endl;
	}
};

class OpticalDiscReader
{
private:

public:
	void launch() 
	{
		 cout << "Optical Disc Reader: launch optical disc reader" <<  endl;
	}
	void checkDisk() 
	{
		 cout << "Optical Disc Reader: check disk" <<  endl;
	}
	void returnToStarting() 
	{
		 cout << "Optical Disc Reader: return to starting" <<  endl;
	}
};

class Computer
{
private:
	PowerSupply* powersupply;
	Sensors* sensors;
	Videocard* videocard;
	RAM* ram;
	HDD* hdd;
	OpticalDiscReader* opticaldiscreader;
public:
	Computer() {
		videocard = new Videocard();
		ram = new RAM();
		hdd = new HDD();
		opticaldiscreader = new OpticalDiscReader();
		powersupply = new PowerSupply();
		sensors = new Sensors();
	}
	~Computer() {
		delete videocard;
		delete ram;
		delete hdd;
		delete opticaldiscreader;
		delete powersupply;
		delete sensors;
	}
	void BeginWork() {
		cout << "Computer ON\n";
		powersupply->toFeed();
		sensors->checkVoltage();
		sensors->checkTemperaturePowerSupply();
		sensors->checkTemperatureVideocard();
		powersupply->toFeedOnVideocard();
		videocard->launch();
		videocard->checkCommunicationMonitor();
		sensors->checkTemperatureRAM();
		powersupply->toFeedRAM();
		ram->launchDevices();
		ram->memoryAnalysis();
		videocard->outputDataRAM();
		powersupply->toFeedOpticalDiscReader();
		opticaldiscreader->launch();
		opticaldiscreader->checkDisk();
		videocard->displayDriveInformation();
		powersupply->toFeedHDD();
		hdd->launch();
		hdd->checkBootSector();
		videocard->outputLaunchHDD();
		sensors->checkTemperatureAllSysteams();
	}
	void EndOfWork() {
		cout << "Computer OFF\n";
		hdd->deviceStop();
		ram->memoryClearing();
		ram->memoryAnalysis();
		videocard->oupputFarewellMessage();
		opticaldiscreader->returnToStarting();
		powersupply->stoptToFeedOnVideocard();
		powersupply->stopToFeedRAM();
		powersupply->stopToFeedOpticalDiscReader();
		powersupply->stopToFeedHDD();
		sensors->checkVoltage();
		powersupply->PowerSupplyOff();
	}
};

void main() {
	Computer computer;
	computer.BeginWork();
	computer.EndOfWork();
}