#include <iostream>
#include "Objects.h"
using namespace std;

int main()
{
	ReceptionRoom* reception = new ReceptionRoom;

	reception->addObject(new WarmColors);
	CoffeeTable* coffetable = new CoffeeTable;
	for (int i = 0; i < 17; i++)
		coffetable->addObject(new Journal);
	reception->addObject(coffetable);
	reception->addObject(new SoftSofa);
	Computer* computer = new Computer;
	computer->addObject(new HDD);
	SecretaryTable* secretarytable = new SecretaryTable;
	secretarytable->addObject(computer);
	secretarytable->addObject(new OfficeTools);
	reception->addObject(secretarytable);
	reception->addObject(new Cooler);
	
	reception->Show();
	delete reception;

	Auditory* auditory1 = new Auditory;
	for (int i = 0; i < 10; i++)
		auditory1->addObject(new Table);
	auditory1->addObject(new Desk);
	TeacherTable* teachertable = new TeacherTable;
	teachertable->addObject(new Computer);
	auditory1->addObject(new Poster);

	auditory1->Show();
	delete auditory1;

	Auditory* auditory2 = new Auditory;
	for (int i = 0; i < 20; i++)
		auditory1->addObject(new BlackTable);
	auditory2->addObject(new Desk);
	auditory2->addObject(new SoftSofa);

	auditory2->Show();
	delete auditory2;

	Auditory* computerauditory = new Auditory;
	for (int i = 0; i < 10; i++)
	{
		Computer* computer = new Computer;
		computer->addObject(new CPU);
		computer->addObject(new Winchester);
		computer->addObject(new RAM);
		computerauditory->addObject(new ComputerTable);
		computerauditory->addObject(computer);
		computerauditory->addObject(new Socket);
	}
	Desk* desk = new Desk;
	desk->addObject(new Markers);
	computerauditory->addObject(desk);

	computerauditory->Show();
	delete computerauditory;
}
