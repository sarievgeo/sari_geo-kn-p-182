#include <iostream>
#include <string>
#include <vector>
using namespace std;


class Object
{
public:
	virtual ~Object() {}
	virtual int GetCost() = 0;
	virtual void Show() = 0;
	virtual void addObject(Object* obj) {}
};

class WarmColors : public Object
{
private:
	int cost = 670;
public:
	WarmColors() {}
	~WarmColors() override {}
	int GetCost() override {
		return cost;
	}
	void Show() override {
		cout << "should be done in warm colors\n";
	}
};

class Table : public Object
{
private:
	vector<Object*> obj;
	int cost = 45;
public:
	Table() {}
	~Table() override
	{
		for (int i = 0; i < obj.size(); i++)
			delete obj[i];
	}

	int GetCost() override
	{
		for (int i = 0; i < obj.size(); i++)
			cost += obj[i]->GetCost();
		return cost;
	}

	void addObject(Object* object) override
	{
		obj.push_back(object);
	}
	void Show()override
	{
		cout << "table\n";
		for (int i = 0; i < obj.size(); i++)
		{
			obj[i]->Show();
		}
	}
};

class CoffeeTable : public Table
{
private:
	vector<Object*> obj;
	int cost = 120;
public:
	CoffeeTable() {}
	~CoffeeTable() override
	{
		for (int i = 0; i < obj.size(); i++)
			delete obj[i];
	}

	void Show()override
	{
		cout << "coffee table\n";
		for (int i = 0; i < obj.size(); i++)
		{
			obj[i]->Show();
		}
	}
};

class Journal : public Object
{
private:
	int cost = 7;
public:
	Journal() {}
	~Journal()override {}
	int GetCost()override
	{
		return cost;
	}
	void Show()override
	{
		std::cout << "magazinece computer world\n";
	}
};

class SoftSofa : public Object
{
private:
	int cost = 532;
public:
	SoftSofa() {}
	~SoftSofa()override {}
	int GetCost()override
	{
		return cost;
	}
	void Show()override
	{
		std::cout << "soft sofa\n";
	}
};

class SecretaryTable : public Table {
private:
	int cost = 320;
	vector<Object*>obj;
public:
	SecretaryTable() {}
	~SecretaryTable()override
	{
		for (int i = 0; i < obj.size(); i++)
			delete obj[i];
	}

	void Show()override
	{
		std::cout << "table Secretary\n";
		for (int i = 0; i < obj.size(); i++) {
			std::cout << "\t";
			obj[i]->Show();
		}
	}
};


class Computer : public Object
{
private:
	int cost = 1020;
	vector<Object*>obj;
public:
	Computer() {}
	~Computer()override
	{
		for (int i = 0; i < obj.size(); i++)
			delete obj[i];
	}
	int GetCost()override {
		for (int i = 0; i < obj.size(); i++) {
			cost += obj[i]->GetCost();
		}
		return cost;
	}
	void Show()override
	{
		std::cout << "computer\n";
		for (int i = 0; i < obj.size(); i++) {
			std::cout << "\t\t";
			obj[i]->Show();
		}
	}
	void addObject(Object* o) override
	{
		obj.push_back(o);
	}
};

class HDD : public Object
{
private:
	int cost = 50;
public:
	HDD() {}
	~HDD()override {}
	int GetCost()override
	{
		return cost;
	}
	void Show()override
	{
		std::cout << "HDD big memory\n";
	}
};

class CPU : public Object
{
private:
	int cost = 150;
public:
	CPU() {}
	~CPU()override {}
	int GetCost()override
	{
		return cost;
	}
	void Show()override
	{
		std::cout << "CPU 2.2Gz\n";
	}
};

class RAM : public Object
{
private:
	int cost = 35;
public:
	RAM() {}
	~RAM()override {}
	int GetCost()override
	{
		return cost;
	}
	void Show()override
	{
		std::cout << "RAM 1024Mb\n";
	}
};

class Winchester : public HDD
{
private:
	int cost = 20;
public:
	Winchester() {}
	~Winchester() override {}
	void Show()override
	{
		std::cout << "Winchester 80Gb\n";
	}
};

class OfficeTools : public Object
{
private:
	int cost = 112;
public:
	OfficeTools() {}
	~OfficeTools()override {}
	int GetCost()override
	{
		return cost;
	}
	void Show()override
	{
		std::cout << "office tools\n";
	}
};

class Cooler : public Object
{
private:
	int cost = 55;
public:
	Cooler() {}
	~Cooler()override {}
	int GetCost()override
	{
		return cost;
	}
	void Show()override
	{
		std::cout << "cooler\n";
	}
};

class Desk : public Object
{
private:
	int cost = 43;
	vector<Object*>obj;
public:
	Desk() {}
	~Desk()override {}
	void addObject(Object* object) override
	{
		obj.push_back(object);
	}
	int GetCost()override
	{
		return cost;
	}
	void Show()override
	{
		std::cout << "desk\n";
	}

};

class TeacherTable : public Table
{
private:
	int cost = 87;
	vector<Object*>obj;
public:
	TeacherTable() {}
	~TeacherTable()override
	{
		for (int i = 0; i < obj.size(); i++)
			delete obj[i];
	}

	void Show()override
	{
		std::cout << "Teacher table\n";
		for (int i = 0; i < obj.size(); i++) {
			std::cout << "\t";
			obj[i]->Show();
		}
	}
};

class ComputerTable : public Table
{
private:
	int cost = 56;
	vector<Object*>obj;
public:
	ComputerTable() {}
	~ComputerTable()override
	{
		for (int i = 0; i < obj.size(); i++)
			delete obj[i];
	}

	void Show()override
	{
		std::cout << "Computer Table\n";
		for (int i = 0; i < obj.size(); i++) {
			std::cout << "\t";
			obj[i]->Show();
		}
	}
};

class Socket : public Object
{
private:
	int cost = 15;
public:
	Socket() {}
	~Socket() override {}

	int GetCost() override
	{
		return cost;
	}
	void Show()override
	{
		cout << "socket\n";
	}
};

class Poster : public Object
{
private:
	int cost = 5;
public:
	Poster() {}
	~Poster() override {}
	int GetCost() override {
		return cost;
	}
	void Show() override {
		cout << "Greate math scientist\n";
	}
};

class BlackTable : public Table
{
private:
	vector<Object*> obj;
	int cost = 68;
public:
	BlackTable() {}
	~BlackTable() override
	{
		for (int i = 0; i < obj.size(); i++)
			delete obj[i];
	}

	void Show()override
	{
		cout << "black table\n";
		for (int i = 0; i < obj.size(); i++)
		{
			obj[i]->Show();
		}
	}
};

class Markers : public Object
{
private:
	int cost = 12;
public:
	Markers() {}
	~Markers()override {}
	int GetCost()override
	{
		return cost;
	}
	void Show()override
	{
		std::cout << "Colorfull Markers\n";
	}
};

class ReceptionRoom : public Object
{
private:
	vector<Object*> obj;
public:
	ReceptionRoom() {}
	~ReceptionRoom()override
	{
		for (int i = 0; i < obj.size(); i++)
			delete obj[i];
	}
	void addObject(Object* o) override
	{
		obj.push_back(o);
	}
	int GetCost()override
	{
		int cost = 0;
		for (int i = 0; i < obj.size(); i++)
		{
			cost += obj[i]->GetCost();
		}
		return cost;
	}
	void Show()override
	{
		for (int i = 0; i < obj.size(); i++)
		{
			obj[i]->Show();
		}
	}
};

class Auditory : public Object
{
private:
	vector<Object*> obj;
public:
	Auditory() {}
	~Auditory() override
	{
		for (int i = 0; i < obj.size(); i++)
			delete obj[i];
	}
	void addObject(Object* o) override
	{
		obj.push_back(o);
	}
	int GetCost()override
	{
		int cost = 0;
		for (int i = 0; i < obj.size(); i++)
		{
			cost += obj[i]->GetCost();
		}
		return cost;
	}
	void Show()override
	{
		for (int i = 0; i < obj.size(); i++)
		{
			obj[i]->Show();
		}
	}
};