#include "DnD.h"

void main() {

	Character* Leon = new DragonKnight(
		new Paladin(
			new Archer(
				new Berserk(
					new Human()))));

	Leon->Show();

	cout << "\n";

	Character* Djaina = new Druid(
		new Necromancer(
			new ArchiMage(
				new Assassin(
					new Elf()))));

	Djaina->Show();
}