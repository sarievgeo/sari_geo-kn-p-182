#include<iostream>

using namespace std;

class Character 
{
protected:
	int Attack = 0;
	int Speed = 0;
	int Health = 0;
	int ArmorClass = 0;
public:
	virtual void SetCharacteristics() = 0;
	virtual void Show() = 0;
};

class Human : public Character 
{
public:
	Human() 
	{
		SetCharacteristics();
	}

	virtual void SetCharacteristics()
	{
		this->Attack += 20;
		this->Speed += 20;
		this->Health += 150;
		this->ArmorClass += 0;
	}

	virtual void Show() 
	{
		cout << "Human" << endl;
		cout << "\tAttack: " << this->Attack << endl;
		cout << "\tSpeed: " << this->Speed << endl;
		cout << "\tHealth: " << this->Health << endl;
		cout << "\tArmorClass: " << this->ArmorClass << endl;
		
	}
};

class Decorator : public Character 
{
private:
	Character* character;
public:
	Decorator(Character* c) : character{ c } {}
	Decorator() {}
	~Decorator() { delete character; }

	void Show() 
	{
		character->Show();
	}
};

class Berserk : public Decorator 
{
public:
	Berserk() {}
	Berserk(Character* c) : Decorator(c) 
	{
		SetCharacteristics();
	}

	void SetCharacteristics() override
	{
		this->Attack = +20;
		this->Speed += 10;
		this->Health += 50;
		this->ArmorClass += 20;
	}

	void Show() 
	{
		Decorator::Show();
		 cout << "Berserk" <<  endl;
		 cout << "\tAttack: " << this->Attack << endl;
		 cout << "\tSpeed: " << this->Speed << endl;
		 cout << "\tHealth: " << this->Health << endl;
		 cout << "\tArmorClass: " << this->ArmorClass << endl;
	}
};

class DecoratorHuman1 : public Berserk 
{
private:
	Berserk* berserk;
public:
	DecoratorHuman1(Berserk* br) : berserk{ br } {}
	DecoratorHuman1() {}
	~DecoratorHuman1() { delete berserk; }

	void Show() 
	{
		berserk->Show();
	}
};

class Paladin : public DecoratorHuman1 
{
public:
	Paladin() {}
	Paladin(Berserk* br) : DecoratorHuman1(br) 
	{
		SetCharacteristics();
	}

	void SetCharacteristics() override
	{
		this->Attack = +40;
		this->Speed += 10;
		this->Health += 50;
		this->ArmorClass += 40;
	}

	void Show() 
	{
		DecoratorHuman1::Show();
		 cout << "Paladin" <<  endl;
		 cout << "\tAttack: " << this->Attack << endl;
		 cout << "\tSpeed: " << this->Speed << endl;
		 cout << "\tHealth: " << this->Health << endl;
		 cout << "\tArmorClass: " << this->ArmorClass << endl;
	}
};

class Archer : public DecoratorHuman1 
{
public:
	Archer(Berserk* br) : DecoratorHuman1(br) 
	{
		SetCharacteristics();
	}

	void SetCharacteristics() override
	{
		this->Attack = +20;
		this->Speed += 20;
		this->Health += 50;
		this->ArmorClass += 10;
	}

	void Show() 
	{
		DecoratorHuman1::Show();
		 cout << "Archer" <<  endl;
		 cout << "\tAttack: " << this->Attack << endl;
		 cout << "\tSpeed: " << this->Speed << endl;
		 cout << "\tHealth: " << this->Health << endl;
		 cout << "\tArmorClass: " << this->ArmorClass << endl;
	}
};

class DecoratorHuman_2 :public Paladin 
{
private:
	Paladin* paladin;
public:
	DecoratorHuman_2(Paladin* pl) : paladin{ pl } {}
	DecoratorHuman_2() {}
	~DecoratorHuman_2() { delete paladin; }

	void Show() 
	{
		paladin->Show();
	}
};

class DragonKnight : public DecoratorHuman_2 
{
public:
	DragonKnight(Paladin* pl) : DecoratorHuman_2(pl) 
	{
		SetCharacteristics();
	}

	void SetCharacteristics() override
	{
		this->Attack = -10;
		this->Speed += 40;
		this->Health += 200;
		this->ArmorClass += 100;
	}	

	void Show() 
	{
		DecoratorHuman_2::Show();
		 cout << "DragonKnight" <<  endl;
		 cout << "\tAttack: " << this->Attack << endl;
		 cout << "\tSpeed: " << this->Speed << endl;
		 cout << "\tHealth: " << this->Health << endl;
		 cout << "\tArmorClass: " << this->ArmorClass << endl;
	}
};


//Line Elf
class Elf : public Character 
{
public:
	Elf() 
	{
		SetCharacteristics();
	}

	virtual void SetCharacteristics()
	{
		this->Attack += 15;
		this->Speed += 30;
		this->Health += 100;
		this->ArmorClass += 0;
	}

	virtual void Show() 
	{
		 cout << "Elf" <<  endl;
		 cout << "\tAttack: " << this->Attack << endl;
		 cout << "\tSpeed: " << this->Speed << endl;
		 cout << "\tHealth: " << this->Health << endl;
		 cout << "\tArmorClass: " << this->ArmorClass << endl;
	}
};

class Assassin : public Decorator 
{
public:
	Assassin() {}
	Assassin(Character* c) : Decorator(c) 
	{
		SetCharacteristics();
	}

	void SetCharacteristics() override
	{
		this->Attack = +20;
		this->Speed -= 10;
		this->Health += 100;
		this->ArmorClass += 20;
	}

	void Show() 
	{
		Decorator::Show();
		 cout << "Assassin" <<  endl;
		 cout << "\tAttack: " << this->Attack << endl;
		 cout << "\tSpeed: " << this->Speed << endl;
		 cout << "\tHealth: " << this->Health << endl;
		 cout << "\tArmorClass: " << this->ArmorClass << endl;
	}
};

class ArchiMage : public Decorator 
{
public:
	ArchiMage() {}
	ArchiMage(Character* c) : Decorator(c) 
	{
		SetCharacteristics();
	}

	void SetCharacteristics() override
	{
		this->Attack = +20;
		this->Speed -= 10;
		this->Health += 100;
		this->ArmorClass += 20;
	}

	void Show() 
	{
		Decorator::Show();
		 cout << "ArchiMage" <<  endl;
		 cout << "\tAttack: " << this->Attack << endl;
		 cout << "\tSpeed: " << this->Speed << endl;
		 cout << "\tHealth: " << this->Health << endl;
		 cout << "\tArmorClass: " << this->ArmorClass << endl;
	}
};

class DecoratorElf_1 : public Assassin 
{
private:
	Assassin* assassin;
public:
	DecoratorElf_1(Assassin* we) : assassin{ we } {}
	DecoratorElf_1() {}
	~DecoratorElf_1() { delete assassin; }

	void Show() 
	{
		assassin->Show();
	}
};
class DecoratorElf_2 : public ArchiMage 
{
private:
	ArchiMage* archimage;
public:
	DecoratorElf_2(ArchiMage* me) : archimage{ me } {}
	DecoratorElf_2() {}
	~DecoratorElf_2() { delete archimage; }

	void Show() 
	{
		archimage->Show();
	}
};

class CrossbowMan : public DecoratorElf_1 
{
public:
	CrossbowMan(Assassin* we) : DecoratorElf_1(we) 
	{
		SetCharacteristics();
	}

	void SetCharacteristics() override
	{
		this->Attack = +20;
		this->Speed += 10;
		this->Health += 50;
		this->ArmorClass -= 10;
	}

	void Show() 
	{
		DecoratorElf_1::Show();
		 cout << "CrossbowMan" <<  endl;
		 cout << "\tAttack: " << this->Attack << endl;
		 cout << "\tSpeed: " << this->Speed << endl;
		 cout << "\tHealth: " << this->Health << endl;
		 cout << "\tArmorClass: " << this->ArmorClass << endl;
	}
};

class Necromancer : public DecoratorElf_2 
{
public:
	Necromancer(ArchiMage* am) : DecoratorElf_2(am) 
	{
		SetCharacteristics();
	}

	void SetCharacteristics() override
	{
		this->Attack = +70;
		this->Speed += 20;
		this->Health += 0;
		this->ArmorClass += 0;
	}

	void Show() 
	{
		DecoratorElf_2::Show();
		 cout << "Necromancer" <<  endl;
		 cout << "\tAttack: " << this->Attack << endl;
		 cout << "\tSpeed: " << this->Speed << endl;
		 cout << "\tHealth: " << this->Health << endl;
		 cout << "\tArmorClass: " << this->ArmorClass << endl;
	}
};


class Druid : public DecoratorElf_2 
{
public:
	Druid(ArchiMage* am) : DecoratorElf_2(am) 
	{
		SetCharacteristics();
	}

	void SetCharacteristics() override
	{
		this->Attack = +50;
		this->Speed += 30;
		this->Health += 100;
		this->ArmorClass += 30;
	}

	void Show() 
	{
		DecoratorElf_2::Show();
		 cout << "Necromancer" <<  endl;
		 cout << "\tAttack: " << this->Attack << endl;
		 cout << "\tSpeed: " << this->Speed << endl;
		 cout << "\tHealth: " << this->Health << endl;
		 cout << "\tArmorClass: " << this->ArmorClass << endl;
	}
};