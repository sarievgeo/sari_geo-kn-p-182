#include <iostream>
using namespace std;

class Herbivore
{
private:
    int weight;
    bool life;
public:
    virtual void eat_grass() = 0;
    virtual ~Herbivore() {}
};

class Bison : public Herbivore
{
public:
    weight = 45;
    void eat_grass() override
    {
        cout << "eat grass";
    }
};

class Wildbeest : public Herbivore
{
public:
    void eat_grass() override
    {
        cout << "eat grass";
    }
};

class Elk : public Herbivore
{
public:
    void eat_grass() override
    {
        cout << "eat grass";
    }
};

class Carnivore
{
private:
    int weight;
    bool life;
    int strenght;
public:
    virtual void eat_meat() = 0;
    virtual ~Carnivore() {}
};

class Wolf : public Carnivore
{
private:
    weight = 30;
    life = true;
    strenght = 15;
public:
    void eat_meat() override
    {
        cout << "eat meat";
    }
};

class Lion : public Carnivore
{
public:
    void eat_meat() override
    {
        cout << "eat meat";
    }
};

class Tiger : public Carnivore
{
public:
    void eat_meat() override
    {
        cout << "eat meat";
    }
};

class Continent 
{
public:
    virtual Herbivore* CreateHerbivore() const = 0;
    virtual Carnivore* CreateCarnivore() const = 0;
};

class North_Amerika : public Continent
{
public:
    Herbivore* CreateHerbivore() const override 
    {
        return new Wildbeest();
    }
    Carnivore* CreateCarnivore() const override
    {
        return new Wolf();
    }
};

class Africa : public Continent
{
    Herbivore* CreateHerbivore() const override
    {
        return new Bison();
    }    
    Carnivore* CreateCarnivore() const override
    {
        return new Lion();
    }
};

class Europ : public Continent
{
    Herbivore* CreateHerbivore() const override
    {
        return new Elk();
    }
    Carnivore* CreateCarnivore() const override
    {
        return new Tiger();
    }
};

class Animal_World
{
public:
    void North_Amerika(Continent *f) 
    {
        Herbivore *
    }
};

int main()
{
    North_Amerika north_amerika;
    Africa africa;
    Europ europ;

    use(&north_amerika);
    use(&africa);
    use(&europ);
}