#include <iostream>

using namespace std;

class Car 
{
private:
	string name;
	string body;
	int engine;
	int wheels;
	string kpp;
public:
	void SetName(const string& n) { name = n; };
	void SetBody(const string& b) { body = b; };
	void SetEngine(const int& e) { engine = e; };
	void SetWheels(const int& w) { wheels = w; };
	void SetKpp(const string& k) { kpp = k; };
};

class CarBuilder
{
protected:
	unique_ptr<Car> car;
public:
	virtual ~CarBuilder() {}
	void createNewCar() { car.reset(new Car); }
	virtual void NameCar() = 0;
	virtual void BodyPart() = 0;
	virtual void EnginePart() = 0;
	virtual void WheelPart() = 0;
	virtual void KPP_AoM() = 0;
	unique_ptr<Car>& GetCar() { return car; }
};

class LanosBuilder : public CarBuilder
{
public:
	LanosBuilder() : CarBuilder() {}
	~LanosBuilder() override {}

	void NameCar() override { car->SetName("Daewoo Lanos"); }
	void BodyPart() override { car->SetBody("Sedan"); }
	void EnginePart() override { car->SetEngine(98); }
	void WheelPart() override { car->SetWheels(13); }
	void KPP_AoM() override { car->SetKpp("5 Maunal"); }
};

class FordBuilder : public CarBuilder
{
public:
	FordBuilder() : CarBuilder() {}
	~FordBuilder() override {}

	void NameCar() override { car->SetName("Ford Probe"); }
	void BodyPart() override { car->SetBody("Cupe"); }
	void EnginePart() override { car->SetEngine(160); }
	void WheelPart() override { car->SetWheels(14); }
	void KPP_AoM() override { car->SetKpp("4 Auto"); }
};

class UAZBuilder : public CarBuilder
{
public:
	UAZBuilder() : CarBuilder() {}
	~UAZBuilder() override {}

	void NameCar() override { car->SetName("UAZ Patriot"); }
	void BodyPart() override { car->SetBody("Universal"); }
	void EnginePart() override { car->SetEngine(120); }
	void WheelPart() override { car->SetWheels(16); }
	void KPP_AoM() override { car->SetKpp("4 Manual"); }
};

class HyundaiBuilder : public CarBuilder
{
public:
	HyundaiBuilder() : CarBuilder() {}
	~HyundaiBuilder() override {}

	void NameCar() override { car->SetName("Hyundai Getz"); }
	void BodyPart() override { car->SetBody("HatchBack"); }
	void EnginePart() override { car->SetEngine(66); }
	void WheelPart() override { car->SetWheels(13); }
	void KPP_AoM() override { car->SetKpp("4 Auto"); }
};


class Shop 
{
private:
	CarBuilder* carbuilder = nullptr;
public:
	~Shop() {}

	void SetCarBuilder(CarBuilder* builder)
	{
		carbuilder = builder;
	}
	unique_ptr<Car>& GetCar() { return carbuilder->GetCar(); }
	
	void ConstructCar() 
	{
		carbuilder->createNewCar();
		carbuilder->NameCar();
		carbuilder->BodyPart();
		carbuilder->EnginePart();
		carbuilder->WheelPart();
		carbuilder->KPP_AoM();
	}

};

void main() 
{
	Shop shop;
	LanosBuilder lanosbuilder;
	shop.SetCarBuilder(&lanosbuilder);
	shop.ConstructCar();
	unique_ptr<Car>& car1 = shop.GetCar();
}