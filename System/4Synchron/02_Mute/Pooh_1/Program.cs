﻿using System;
using System.Threading;

namespace NoMoneyNoHoney
{
    class Program
    {
        private static Semaphore semaphore;
        private static int honey = 20;
        static void AddNewBee()
        {
            semaphore.WaitOne();
            Random random = new Random();
            Console.WriteLine($"{Thread.CurrentThread.Name} gatharing...");
            Thread.Sleep(random.Next(2000));
            Console.WriteLine($"{Thread.CurrentThread.Name} returned.");
            honey += 10;
            Console.WriteLine(honey);
            semaphore.Release();
        }

        static void Main(string[] args)
        {

            semaphore = new Semaphore(initialCount: 5, maximumCount: 5);

            var threads = new Thread[15];
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(AddNewBee);
                threads[i].Name = $"Bee #{i + 1}";

                Thread.Sleep(50);

                threads[i].Start();
            }

            for (int i = 0; i < threads.Length && honey > -30; i++)
            {
                threads[i].Join();
                if (honey > -30)
                {
                    honey -= 30;
                }
            }
            if (honey <= -30)
            {
                Console.WriteLine("Pooh patuh...");
            }
            Console.ReadKey(true);
        }
    }
}
