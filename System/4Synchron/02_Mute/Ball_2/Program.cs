﻿using System;
using System.Threading;

namespace SemaphoreSample
{
    class Program
    {
        private static Semaphore semaphore;
        static void Roll()
        {
            Random random = new Random();
            int X = 0;
            int Y = 0;
            while (X <= 15 && Y <= 15)
            {
                Thread.Sleep(random.Next(20));
                X += random.Next(2);
                Y += random.Next(2);
            }
        }

        static void AddNewBall()
        {
            semaphore.WaitOne();

            Console.WriteLine($"{Thread.CurrentThread.Name} rouling...");
            Thread thread = new Thread(Roll);
            thread.Start();
            semaphore.Release();
            thread.Join();
            Console.WriteLine($"{Thread.CurrentThread.Name} fall.");
        }

        static void Main(string[] args)
        {

            semaphore = new Semaphore(initialCount: 5, maximumCount: 5);

            var threads = new Thread[15];
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(AddNewBall);
                threads[i].Name = $"Ball #{i + 1}";

                Thread.Sleep(50);

                threads[i].Start();
            }

            for (int i = 0; i < threads.Length; i++)
                threads[i].Join();

            Console.WriteLine("Press any key...");
            Console.ReadKey(true);
        }
    }
}
