﻿using System;
using System.Threading;

namespace SemaphoreSample
{
    class Program
    {
        private static Semaphore semaphore;
        private static int[,] MyMap = new int[5, 5];
        private static int inc = 0;
        static void AddNewAvio()
        {
            Random random = new Random();
            semaphore.WaitOne();
            int j = inc;
            int found = 0;
            Console.WriteLine($"{Thread.CurrentThread.Name} is starting...");
            for(int i = random.Next(5); i < 5; i++)
            {
                if(MyMap[j, i] == 1)
                {
                    found++;
                }
            }
            Console.WriteLine($"Found: {found}");
            Console.WriteLine($"{Thread.CurrentThread.Name} finished.");

            semaphore.Release();
        }

        static void Main(string[] args)
        {
            semaphore = new Semaphore(initialCount: 5, maximumCount: 5);
            Random random = new Random();
            int rand = 0;
            int Peh = random.Next(25);
            while (Peh > 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        rand = random.Next(50) % 2;
                        if (rand == 1)
                        {
                            MyMap[i, j] = 1;
                            Peh--;
                        }
                    }
                }
            }
            var threads = new Thread[5];
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(AddNewAvio);
                threads[i].Name = $"Checker #{i + 1}";
                
                Thread.Sleep(50);

                threads[i].Start();

            }

            for (int i = 0; i < threads.Length; i++)
            {
                threads[i].Join();
                if (inc < 5)
                    inc++;
            }
            Console.WriteLine("Press any key...");
            Console.ReadKey(true);
        }
    }
}
