﻿using System;
using System.Threading;

namespace SemaphoreSampleBB
{
    class Program
    {
        private static Semaphore semaphore;
        private static int Red = 500, Blue = 500;
        static void BattleRed()
        {

            semaphore.WaitOne();
            while (Red > 0 && Blue > 00)
            {
                Random random = new Random();
                int kills = random.Next(50);
                Blue -= kills;
                Console.WriteLine($"{Thread.CurrentThread.Name} Killed: {kills}");
                int recruit = random.Next(20);
                Red += recruit;
                Console.WriteLine($"{Thread.CurrentThread.Name} Recruited: {recruit}");
                Console.WriteLine($"Red team: {Red}");
                Console.WriteLine($"Blue team: {Blue}");
            }
           
            semaphore.Release();
        }

        static void BattleBlue()
        {
            semaphore.WaitOne();
            while (Red > 0 && Blue > 00)
            {
                Random random = new Random();
                int kills = random.Next(50);
                Red -= kills;
                Console.WriteLine($"{Thread.CurrentThread.Name} Killed: {kills}");
                int recruit = random.Next(20);
                Blue += recruit;
                Console.WriteLine($"{Thread.CurrentThread.Name} Recruited: {recruit}");
                Console.WriteLine($"Blue team: {Blue}");
                Console.WriteLine($"Red team: {Red}");
            }
            
            semaphore.Release();
        }

        static void Main(string[] args)
        {
            semaphore = new Semaphore(initialCount: 2, maximumCount: 2);

            var threads = new Thread[2];
               
            threads[0] = new Thread(BattleRed);
            threads[0].Name = $"Player #{1}";
            threads[0].Start();
            threads[1] = new Thread(BattleBlue);
            threads[1].Name = $"Player #{2}";
            threads[1].Start();
            Thread.Sleep(50);


            for (int i = 0; i < threads.Length; i++)
                threads[i].Join();
            if(Blue > 0)
            {
                Console.WriteLine($"Blue Wins!");
            }
            else
                Console.WriteLine($"Red Wins!");
            Console.WriteLine("Press any key...");
            Console.ReadKey(true);
        }
    }
}
