﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;

namespace Threaders
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public class MyThread1
    {
        private double i = 0.5;
        public void Run()
        {
            i += 0.5;
            //First.AppendText(i.ToString());
            Thread.Sleep(500);
        }

    }
    public class MyThread2
    {
        public void Run()
        {

        }

    }
    public class MyThread3
    {
        public void Run()
        {

        }

    }
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var thread1 = new Thread(new ThreadStart(new MyThread1().Run));
            var thread2 = new Thread(new ThreadStart(new MyThread2().Run));
            var thread3 = new Thread(new ThreadStart(new MyThread3().Run));
            thread1.Start();
            thread2.Start();
            thread3.Start();
        }
    }
}
